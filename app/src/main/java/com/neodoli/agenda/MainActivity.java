package com.neodoli.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ListView AgendaList=null;
   DataAcess dataAcess;
    ArrayList<ActividadeAgenda> listaActividades;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataAcess= new DataAcess(this);
        dataAcess.open();

        AgendaList= (ListView) findViewById(R.id.lv);

        Button add= (Button) findViewById(R.id.add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in= new Intent(getApplicationContext(), AddActivity.class);
                startActivity(in);


            }
        });

        /**
         *  Implemetando o metodo de escuta de cliques sobre os itens no ListView
         *  este é chamado quando um determinado intem dentro lista é clicado.
         *  */
        AgendaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ActividadeAgenda act= (ActividadeAgenda) parent.getItemAtPosition(position);
                Intent inte= new Intent(getApplicationContext(), ViewActivity.class);
                inte.putExtra("id", act.getId());

                startActivity(inte);

            }
        });

        this.listaActividades= this.lerDados();
        AgendaAdapter a= new AgendaAdapter(listaActividades, getApplicationContext());

        AgendaList.setAdapter(a);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in= new Intent(getApplicationContext(), AddActivity.class);
                startActivity(in);
            }
        });
    }


    /**
     *  Metodo que sera utilizado para introduzir os dados no model
     *  os dados serao introduzidos de forma estatica.
     *  */
    private ArrayList<ActividadeAgenda> lerDados(){

        //new ActividadeAgenda("Android Study Jam","formacao em android basico", "UCM", new Date().toString());
        //this.dataAcess.insertActivity( new ActividadeAgenda("Android Study Jam","formacao em android basico", "UCM", new Date().toString()));
        //this.dataAcess.insertActivity(new ActividadeAgenda("Android Study Jam", "formacao em android basico", "UCM", new Date().toString()));


      ArrayList<ActividadeAgenda> actividades= this.dataAcess.todasActividades();


       /** ArrayList<ActividadeAgenda> actividades= new  ArrayList<ActividadeAgenda>();

        actividades.add(new ActividadeAgenda("Android Study Jam","formacao em android basico", "UCM", new Date().toString()));
        actividades.add(new ActividadeAgenda("Android Study Jam","formacao em android basico", "UCM", new Date().toString()));
        actividades.add(new ActividadeAgenda("Android Study Jam","formacao em android basico", "UCM", new Date().toString()));

        */

        return actividades;

    }

    @Override
    protected void onResume() {
        super.onResume();

      this.dataAcess.open();
        this.listaActividades= this.lerDados();
    }

    @Override
    protected void onPause() {
        super.onPause();
      this.dataAcess.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
     this.dataAcess.close();
    }
}
