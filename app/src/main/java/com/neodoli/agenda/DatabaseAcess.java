package com.neodoli.agenda;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by DEMAUSER on 5/27/2016.
 */
public class DatabaseAcess extends SQLiteOpenHelper{

    public final static String DATABASE_NAME="data.db";
    public final static String TABLE_NAME="actividades";
    public final static String COLUN_TITLE="title";
    public final static String COLUN_PLACE="place";
    public final static String COLUN_DESCRIPTION="description";
    public final static String COLUN_DATE="date";
    public final static String COLUN_ID="_id";
    public final static int DATABASE_VERSION=1;


    public final static String TABLE_DELETE="drop table "+ TABLE_NAME;
    public final static String TABLE_CREATE="create table "+TABLE_NAME+" ( "+COLUN_ID+" integer primary key autoincrement, "+
                                            COLUN_TITLE+" text not null,"+COLUN_PLACE+" text not null,"+COLUN_DESCRIPTION+" text not null, "+
                                            COLUN_DATE+" text not null);";



        /**create to initiate the father contructor*/
        public DatabaseAcess(Context context) {
            /**
             *  primeiro parametro do contrutor representa o contexto em que o componente vai ser executador
             *  name -  reprsenta o nome do banco de dados
             *  factory -
             *  version -  a versao do banco de dados
             * */
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(TABLE_DELETE);
        db.execSQL(TABLE_CREATE);


    }
}
