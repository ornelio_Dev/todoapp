package com.neodoli.agenda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Date;

public class EditActivity extends AppCompatActivity {

    DataAcess dataAcess;

    EditText lugar, descricao, titulo;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        this.dataAcess= new DataAcess(this);
        dataAcess.open();

        Intent intent= getIntent();
        final Bundle dados=intent.getExtras();

        lugar= (EditText) findViewById(R.id.ed_local);
        lugar.setText(dados.getString("place"));

        descricao= (EditText) findViewById(R.id.ed_descricao);
        descricao.setText(dados.getString("descricao"));

        titulo= (EditText) findViewById(R.id.ed_titulo);
        titulo.setText(dados.getString("titulo"));

        bt= (Button) findViewById(R.id.bt_alterar);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ActividadeAgenda act= new ActividadeAgenda(titulo.getText().toString(), descricao.getText().toString(), lugar.getText().toString(), new Date().toString());
                act.setId(dados.getInt("id"));
                dataAcess.updateActivity(act) ;

                lugar.setText("");
                descricao.setText("");
                titulo.setText("");

                Intent intent1= new Intent(getApplicationContext(), ViewActivity.class);
                intent1.putExtra("id",dados.getInt("id"));

                startActivity(intent1);


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        this.dataAcess.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.dataAcess.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.dataAcess.close();
    }
}
