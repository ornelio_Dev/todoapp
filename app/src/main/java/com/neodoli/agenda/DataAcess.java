package com.neodoli.agenda;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by DEMAUSER on 5/28/2016.
 */
public class DataAcess {

    private DatabaseAcess databaseAcess;
    private SQLiteDatabase db;

   public  DataAcess( Context context) {

       this.databaseAcess = new DatabaseAcess(context);
    }
    public void close(){
        this.databaseAcess.close();
    }

    public void open(){
     this.db= this.databaseAcess.getWritableDatabase();
    }

    public  ArrayList<ActividadeAgenda> todasActividades (){

        ArrayList<ActividadeAgenda> all= new ArrayList<ActividadeAgenda>();

        /** parametros do method query
         * primeiro string-  nome da tabela
         * segundo string[]-  nome dos campos que deseja retornar, quando passado null retorn todos campos.
         *terceiro string[]- usado para definir a clausula where quando passado null retorna todas linhas.
         * quarto string -  usado para filter os campos a serem apresetados usando placeholder ?
         * quinto string -  usado para definir groupby
         * sexto String -  usado para heaving
         * setimos string - usado ordenar orderby
         *
         * */

        Cursor cursor = this.db.query(this.databaseAcess.TABLE_NAME,null,null,null,null,null,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            ActividadeAgenda act= new ActividadeAgenda(cursor.getString(1),cursor.getString(3), cursor.getString(2), cursor.getString(4));
            act.setId(cursor.getInt(0));
            all.add(act);
            cursor.moveToNext();

        }

        cursor.close();
        return all;
    }

    public ActividadeAgenda Actividade (int id){

        String where=DatabaseAcess.COLUN_ID+" = "+id;

        Cursor cursor = this.db.query(this.databaseAcess.TABLE_NAME,null,where,null,null,null,null);
        cursor.moveToFirst();
        ActividadeAgenda act=null;
        act= new ActividadeAgenda(cursor.getString(1),cursor.getString(3), cursor.getString(2), cursor.getString(4));
        act.setId(cursor.getInt(0));
        cursor.moveToNext();

        cursor.close();
        return act;

    }

    public void insertActivity(ActividadeAgenda act){

        String title=act.getTitle();
        String place=act.getPlace();
        String description=act.getDescription();
        String date=act.getDate();

        ContentValues contentValues=  new ContentValues();
        contentValues.put(DatabaseAcess.COLUN_TITLE, title);
        contentValues.put(DatabaseAcess.COLUN_DESCRIPTION, description);
        contentValues.put(DatabaseAcess.COLUN_PLACE, place);
        contentValues.put(DatabaseAcess.COLUN_DATE, date);
        /**content values arguments
         * primeiro o nome da tabela
         * segundo lista de campos a ser adicionados
         * terceiro os valores a serem adicionados nos campos.
         * */

        String [] filds = {DatabaseAcess.COLUN_TITLE,DatabaseAcess.COLUN_PLACE,DatabaseAcess.COLUN_DESCRIPTION,DatabaseAcess.COLUN_DATE };
        this.db.insert(this.databaseAcess.TABLE_NAME, null,contentValues );
    }

    public void deleteActivity(int id){
        String where=databaseAcess.COLUN_ID+"="+id;
        this.db.delete(databaseAcess.TABLE_NAME, where, null);
    }

    public void updateActivity(ActividadeAgenda act){

        ContentValues contentValues= new ContentValues();
        String title=act.getTitle();

        String place=act.getPlace();

        String description=act.getDescription();

        String date=act.getDate();

        int id= act.getId();


        String where= databaseAcess.COLUN_ID+"="+id;


        contentValues.put(DatabaseAcess.COLUN_PLACE, place);
        contentValues.put(DatabaseAcess.COLUN_DESCRIPTION, description);
        contentValues.put(DatabaseAcess.COLUN_TITLE, title);
        contentValues.put(DatabaseAcess.COLUN_DATE, date);

        this.db.update(databaseAcess.TABLE_NAME,contentValues, where, null);
    }

}
