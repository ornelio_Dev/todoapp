package com.neodoli.agenda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ViewActivity extends AppCompatActivity {

    TextView tvLocal;
    TextView tvTitulo;
    TextView tvDescription;
    TextView tvDate;
    Button btEliminar, btEditar;


     DataAcess dataAcess;

    ActividadeAgenda act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        this.dataAcess= new DataAcess(this);
        dataAcess.open();

        Intent inte= getIntent();

       int id= inte.getExtras().getInt("id");


         act= dataAcess.Actividade(id);


        btEditar= (Button) findViewById(R.id.bt_editar);

        btEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent inte= new Intent(getApplicationContext(), EditActivity.class);

                inte.putExtra("titulo", act.getTitle());
                inte.putExtra("place", act.getPlace());
                inte.putExtra("descricao", act.getDescription());
                inte.putExtra("date", act.getDate());
                inte.putExtra("id", act.getId());

                startActivity(inte);

            }
        });

        btEliminar= (Button) findViewById(R.id.bt_eliminar);
        btEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dataAcess.deleteActivity(act.getId());
                Intent inte = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(inte);
            }
        });

        tvLocal= (TextView) findViewById(R.id.tx_local);
        tvLocal.setText(act.getPlace());

        tvTitulo= (TextView) findViewById(R.id.tx_titulo);
        tvTitulo.setText(act.getTitle());

        tvDescription= (TextView) findViewById(R.id.tx_descricao);
        tvDescription.setText(act.getDescription());

        tvDate= (TextView) findViewById(R.id.tx_data);
        tvDate.setText(act.getDate());



    }

    @Override
    protected void onResume() {
        super.onResume();

        this.dataAcess.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.dataAcess.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.dataAcess.close();
    }
}
