package com.neodoli.agenda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Date;

public class AddActivity extends AppCompatActivity {

    DataAcess dataAcess;

    EditText lugar, descricao, titulo;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        this.dataAcess= new DataAcess(this);
        dataAcess.open();

        lugar= (EditText) findViewById(R.id.ed_local);
        descricao= (EditText) findViewById(R.id.ed_descricao);
        titulo= (EditText) findViewById(R.id.ed_titulo);
        bt= (Button) findViewById(R.id.act_add);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dataAcess.insertActivity(new ActividadeAgenda(titulo.getText().toString(), descricao.getText().toString(), lugar.getText().toString(), new Date().toString()));

                lugar.setText("");
                descricao.setText("");
                titulo.setText("");


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        this.dataAcess.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.dataAcess.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.dataAcess.close();
    }
}
